public class ProcessedData {
	
	double corr;
	Data data;
	
	public ProcessedData(Data data, Double corr)
	{
		this.data = data;
		this.corr = corr;
	}
	
	public double getCorr()
	{
		return corr;
	}
	
	public Data getData()
	{
		return data;
	}

	public void setCorr(double corr)
	{
		this.corr = corr;
	}
	
	public void setData(Data data)
	{
		this.data = data;
	}
}

