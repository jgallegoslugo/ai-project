import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class StartFrame extends JFrame
{
	private static final long serialVersionUID = 1234L;
	private static int width = 300, height = 500;
	private JButton startButton;
	private JButton databaseButton;
	private ButtonListener listener;
	private BackgroundPanel background;
	private JPanel middle, top, bottom;
	private JLabel backgroundImage;
	String filename = System.getProperty("user.dir") + "/images/bgImage.png";
	private ImageIcon img = Data.resizeImageIcon(new ImageIcon(filename), width, height);
	
	public StartFrame()
	{
		this(width, height);
	}
	
	public StartFrame(int width, int height)
	{
		//for JFrame
		setLocation(new Point(300, 100));
		setPreferredSize(new Dimension(width, height));
		setTitle("Note Taker");
		
		//background panel in which to place dummy panels
		backgroundImage = new JLabel();
		backgroundImage.setIcon(Data.resizeImageIcon(img, width-20, height+100));
		
		background = new BackgroundPanel(new FlowLayout());
		background.setPreferredSize(new Dimension(width, height));
		
		
		//dummy panels so that we can center the panel with buttons on it
		top = new JPanel(new FlowLayout());
		top.setPreferredSize(new Dimension(width, height/4));
		top.setOpaque(false); //make background transparent
		
		
		bottom = new JPanel(new FlowLayout());
		bottom.setPreferredSize(new Dimension(width, height/3));
		bottom.setBackground(Color.WHITE);
		bottom.setOpaque(false);
		
		middle = new JPanel(new FlowLayout());
		middle.setBackground(Color.WHITE);
    middle.setPreferredSize(new Dimension(width - 100, height - height/3));
    middle.setOpaque(false);
    
    background.add(top, "North");
    background.add(middle, "Center");
    background.add(bottom, "South");
    
    //for button clicks
		listener = new ButtonListener();
		
		//buttons
		startButton = new JButton("Start");
		databaseButton = new JButton("Create");
		
		startButton.setPreferredSize(new Dimension(100, 50));
		databaseButton.setPreferredSize(new Dimension(100, 50));
		
		startButton.addActionListener(listener);
		databaseButton.addActionListener(listener);
		
		//add panels and buttons to frame
		middle.add(startButton, "North");
		middle.add(databaseButton, "South");
		getContentPane().add(background);
		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	//listenet for buttons
	private class ButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event) {
     	String buttonClicked = event.getActionCommand();
     	
     	if(buttonClicked.equals("Start"))
     	{
     		Main.program.display();
     		Main.recognizer.setLocation(500, 0);
     		Main.recognizer.display();
     	}
     	else if(buttonClicked.equals("Create"))
     	{
     		Main.creator.display();
     	}
     	
		}
	}
	
	//Overridden JPanel so that we can add a background image.
	//This is needed because we have panels upon panels.
	private class BackgroundPanel extends JPanel
  {
      private static final long serialVersionUID = 4444L;

      public BackgroundPanel(FlowLayout layout)
      {
      	super(layout);
      }
      @Override
      public void paintComponent(Graphics g)
      {
          g.drawImage(img.getImage(), 0, 0, null);
      }
  };
}
