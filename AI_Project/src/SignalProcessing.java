import java.awt.Point;
import java.util.List;
import java.util.ArrayList;
import java.lang.Math;

import java.io.Serializable;
public class SignalProcessing implements Serializable {

	private static final long serialVersionUID = 2L;
	
	SignalProcessing()
	{
		
	}
	
	public List<Double> distanceArray(List<Point> points, Point midPoint)
	{
		return distanceArray(points, (int)midPoint.getX(), (int)midPoint.getY());
	}
	
	public List<Double> distanceArray(List<Point> points, int midpointX, int midpointY )
	{
		List<Double> distances = new ArrayList<Double>(points.size());
		double dist = 0.0, radical = 0.0;
		
		for(int i = 0; i < points.size(); i++)
		{
			radical = Math.pow((points.get(i).x - midpointX), 2) + Math.pow((points.get(i).y - midpointY), 2) ;
			dist = Math.sqrt(radical);
			distances.add(i, dist);
		}
		
		return distances;
	}
	
	//in order for this method to work correctly, List A must be
	//bigger than List B. In other words, A.size() > B.size()
	public void padding(List<Double> A, List<Double> B)
	{
		List<Double> temp = A;
		if(A.size() > B.size())
		{
			temp = pad(A, B);
			B.clear(); B.addAll(temp);
		}
		else if(B.size() > A.size())
		{
			temp = pad(B, A);
			A.clear(); A.addAll(temp);
		}
	}
	
	public List<Double> pad(List<Double> A, List<Double> B)
	{
		List<Double> paddedArray = new ArrayList<Double>(A.size());
		double ratio = B.size()*1.0/A.size();
		List<Double> indexes = new ArrayList<Double>(A.size());
		
		for(int i = 0; i < A.size(); i++)
		{
			indexes.add((double)Math.round(i*ratio));
		}
		
		int last = (int)(indexes.get(A.size()-1) < A.get(A.size() - 1) ? Math.round(indexes.get(A.size() - 1)) - 1 : Math.round(A.get(A.size() - 1) - 1 ));
		
		indexes.remove(A.size() - 1);
		indexes.add((double)last);
		
		for(int t = 0; t < A.size(); t++)
		{
			paddedArray.add(t, B.get((int)(Math.round(indexes.get(t)))));
		}
		
		return paddedArray;
	}
	
	public double maxCorr(List<Double> A, List<Double> B)
	{
		@SuppressWarnings({ "unchecked", "rawtypes" })
		List<Double> shiftedB = new ArrayList((ArrayList<Double>)B);
		Double val = 0.;
		Double[] rho = new Double[A.size()];
		int end = B.size();
		
		for(int i = 0; i < A.size(); i++)
		{
			rho[i] = corr(i, A, shiftedB);
			val = shiftedB.remove(0);
			shiftedB.add(end, val);
		}
		
		return max(rho);
	}
	
	public double max(Double[] list)
	{
		double max = list[0];
		
		for(int i = 1; i < list.length; i++)
		{
			if(Math.abs(list[i]) > Math.abs(max))
				max = Math.abs(list[i]);
		}
		
		return max;
	}
	
	public double corr(int shift, List<Double> A, List<Double> B)
	{
		padding(A, B);
		
		return xcorr(A, B);
	}
	
	public Point midPoint(List<Point> points)
	{
		double midX = 0., midY = 0.;
		int size = points.size();
		
		for(int i = 0; i < size; i++)
		{
			midX += points.get(i).getX();
			midY += points.get(i).getY();
		}
		
		Point midPoint = new Point((int)midX/size, (int)midY/size);
		
		return midPoint;
	}
	
	private double xcorr(List<Double> A, List<Double> B)
	{
		int size =A.size();// Math.min(A.size(), B.size());
		double sAA = 0., sBB = 0., sAB = 0;
		double sumA = 0., sumB = 0.;
		double varianceAA = 0, varianceBB = 0, varianceAB = 0.;
		
		for(int i = 0; i < size; i++)
		{
			sumA += A.get(i);
			sumB += B.get(i);
		}
		
		varianceAA = sumA*sumA/size;
		varianceBB = sumB*sumB/size;
		varianceAB = sumA*sumB/size;
		
		for(int i = 0; i < size; i++)
		{
			sAA += A.get(i)*A.get(i);
			sBB += B.get(i)*B.get(i);
			sAB += A.get(i)*B.get(i);
		}
		
		sAA -= varianceAA;
		sBB -= varianceBB;
		sAB -= varianceAB;
		
		return sAB/Math.sqrt(sAA*sBB);
	}
	
	private double sincInterpolation(List<Double> B, int t, double T)
	{
		double sum = 0;
		
		for(int n = 0; n < B.size(); n++)
		{
			sum += B.get(n)*sinc(t,n, T);
		}
		
		return sum;
	}
	
	private double sinc(int t, int n, double T)
	{
		return (t-n*T) == 0? 1: Math.sin(Math.PI*(t-n*T)/T)/(Math.PI*(t-n*T)/T);
	}
	
}
