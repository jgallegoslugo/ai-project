import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.event.*;

public class DrawingFrame {
    
		private static final boolean PRETTY = true;  // true to anti-alias
    
    public int width, height;    // dimensions of window frame
    public JFrame frame;         // overall window frame
    private JPanel panel;         // overall drawing surface
    private BufferedImage image;  // remembers drawing commands
    private Graphics2D g2;        // graphics context for painting
    private JLabel statusBar;     // status bar showing mouse position  
    static int ox, oy;

    JPanel buttonPanel;
    
    public final int DELAY = 2500;  // delay between repaints in millis
		public List<Point> points = new ArrayList<Point>();
		long startTime=0;
		long endTime=0;

    public DrawingFrame(int width, int height, JPanel buttonPanel, String title) {
    	
    		this.buttonPanel = buttonPanel;
        this.width = width;
        this.height = height;
        this.image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        
        this.statusBar = new JLabel(" ");
        this.statusBar.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        
        panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.setPreferredSize(new Dimension(width, height));
        panel.add(new JLabel(new ImageIcon(image)));
        
        // listen to mouse movement
        MouseInputAdapter listener = new Adapter();
        panel.addMouseListener(listener);
        panel.addMouseMotionListener(listener);
        
        g2 = (Graphics2D)image.getGraphics();
        g2.setColor(Color.BLACK);
        
        if (PRETTY) 
        {
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setStroke(new BasicStroke(1.1f));
        }
        
        this.frame = new JFrame(title);
        
        this.frame.setResizable(false);
        this.frame.getContentPane().add(panel);
        this.frame.getContentPane().add(buttonPanel, "North");
        this.frame.getContentPane().add(statusBar, "South");
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);     
        
    }
    
    public void setLocation(int x, int y)
    {
    	this.frame.setLocation(x,y);
    }
    public void display()
    {
    	this.frame.pack();
    	this.frame.setVisible(true);
      this.frame.revalidate();
    }
    
    public void setVisible(boolean bool)
    {
    	frame.setVisible(bool);
    }
    public void drawImage(ImageIcon img, int x, int y)
    {
    	img.paintIcon(panel, g2, x, y);
    }
    public void clearPanel()
    {
    	points.clear();
    	panel.repaint();
    }
    
    public void closeFrame()
    {
    	frame.dispose();
    }
    
    // obtain the Graphics object to draw on the panel
    public Graphics2D getGraphics() {
        return g2;
    }
    
    private class Adapter extends MouseInputAdapter
    {
		
			public void mouseMoved(MouseEvent e) 
	    {
	        ox=-1;oy=-1;
	        DrawingFrame.this.statusBar.setText("(" + e.getX() + ", " + e.getY() + ")");
	    }
	   
	    public void mousePressed(MouseEvent e)
	    {
	    	if(startTime==0)
	    	{
	    		startTime=System.currentTimeMillis();
	    		//System.out.println("set startTime: " + startTime);
	    	}
	   
	    	endTime= System.currentTimeMillis();
	    	if(endTime-startTime>=2000)
	    	{
	    		//buttonPanel.setPoints(points);
	    		points.clear();
	    		panel.repaint();
	    	}
	    }
	    
	    public void mouseDragged(MouseEvent e)
			{	
				Container c=(Container)e.getSource();//Container=e.getSource()
				g2=(Graphics2D) c.getGraphics();//Container.getGraphics
				if (ox>=0) 
				{
					//g2.drawOval(e.getX(), e.getY(), 2, 2);
					g2.drawLine(ox,oy,e.getX(),e.getY());

					//if the distance between the last point and the current point
					//is greater than five px, create more points in between
					if(Point.distance(ox, oy, e.getX(), e.getY()) > 6)
					{
						List<Point> morePoints = makeMorePoints(e.getPoint(), new Point(ox, oy)); 
						points.addAll(morePoints);

						for(int i = 0; i < morePoints.size(); i++)
						{
							//g2.drawOval((int)morePoints.get(i).getX(), (int)morePoints.get(i).getY(), 2, 2);
						}
					}
				}
			
				if((ox == -1 && oy == -1) || Point.distance(e.getX(), e.getY(), ox, oy) > 2)
				{
					ox=e.getX(); oy=e.getY();
					points.add(new Point(ox,oy));
					//System.out.println(ox + " " + oy);
				}
			}
	    
	    public void mouseReleased(MouseEvent e)
	    {
	    	startTime = System.currentTimeMillis();
	    }
	    
	    public void mouseExited(MouseEvent e) {
	    	DrawingFrame.this.statusBar.setText(" ");
	    }
	    
	    private List<Point> makeMorePoints(Point current, Point previous)
			{
				List<Point> morePoints = new ArrayList<Point>();
				int numPoints = (int)current.distance(previous)/3;
				numPoints = numPoints == 0? 1: numPoints;
				double xIncrement = (current.getX() - previous.getX())/numPoints;
				double yIncrement = (current.getY() - previous.getY())/numPoints;
				double nextX = previous.getX() + xIncrement;
				double nextY = previous.getY() + yIncrement ;

				for(int i = 0; i < numPoints; i++)
				{
					morePoints.add(new Point((int)Math.round(nextX), (int)Math.round(nextY)));
					nextX += xIncrement;
					nextY += yIncrement;
				}

				return morePoints;
			}
    }
}

