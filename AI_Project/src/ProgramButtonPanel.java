import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class ProgramButtonPanel extends JPanel {

	private static final long serialVersionUID = 1111L;
	JButton clearScreenButton;
	JButton correlate;
	JButton selection4;
	JButton loadButton, addKeyButton, addImageButton, retryButton;
	ArrayList<Data> listData;
	JButton[] button = new JButton[3];
	String[] strings = new String[3];
	Matcher matcher;
	
	
	public ProgramButtonPanel()
	{
		loadButton = new JButton("Load Database");
		clearScreenButton = new JButton("Clear Screen");
		correlate = new JButton("Correlate");
		button[0] = new JButton("");
		button[0].setName("one");
		button[1] = new JButton("");
		button[1].setName("two");
		button[2] = new JButton("");
		button[2].setName("three");
		selection4 = new JButton("No Match");
		selection4.setName("No Match");
		addKeyButton = new JButton("Add Key");
		addKeyButton.setName("Add Key");
		addImageButton = new JButton("Add Image");
		addImageButton.setName("Add Image");
		retryButton = new JButton("Retry");
		retryButton.setName("Retry");

		add(loadButton);
		add(clearScreenButton);
		add(correlate);
		add(button[0]);
		add(button[1]);
		add(button[2]);
		add(selection4);
		add(addKeyButton);
		add(addImageButton);
		add(retryButton);
		
		button[0].setVisible(false);
		button[1].setVisible(false);
		button[2].setVisible(false);
		selection4.setVisible(false);
		addKeyButton.setVisible(false);
		addImageButton.setVisible(false);
		retryButton.setVisible(false);
		
		ActionListener  buttonListener = new ButtonListener();
		 
		loadButton.addActionListener(buttonListener);
		clearScreenButton.addActionListener(buttonListener);
		correlate.addActionListener(buttonListener);
		button[0].addActionListener(buttonListener);
		button[1].addActionListener(buttonListener);
		button[2].addActionListener(buttonListener);
		selection4.addActionListener(buttonListener);
		addKeyButton.addActionListener(buttonListener);
		addImageButton.addActionListener(buttonListener);
		retryButton.addActionListener(buttonListener);
}
	
	private class ButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
     	String buttonClicked = event.getActionCommand();
     	JButton choiceButton = (JButton)event.getSource();
     	
     	if(buttonClicked.equals("Load Database"))
     	{
     		Main.readDatabase();
     	}
     	
     	else if(buttonClicked.equals("Clear Screen"))
     	{
     		Main.recognizer.clearPanel();
     	}
     	
     	else if(buttonClicked.equals("Correlate"))
     	{
     		if(Main.recognizer.points.size() == 0)
     		{
     			JOptionPane.showMessageDialog(null, "Draw an image first");
     		}
     		else
     		{
		   		Data data = new Data(Main.recognizer.points);
		   		matcher = new Matcher(data);
		   		setButtonText();
     		}
     	}
     	
     	else if(buttonClicked.equals("No Match"))
     	{
     		noMatch();
     	}
     	
     	else if(choiceButton.getName().equals("one"))
     	{
     		if(listData.get(0).hasKey())
     		{
     			Main.program.drawKey(listData.get(0).getKey());
     			reInitialized();
     		}
     		else
     		{
     			Main.program.drawImage(listData.get(0).getImage());
     			reInitialized();
     		}
     	}
     	
     	else if(choiceButton.getName().equals("two"))
     	{
     		if(listData.get(1).hasKey())
     		{
     			Main.program.drawKey(listData.get(1).getKey());
     			reInitialized();
     		}
     		else
     		{
     			Main.program.drawImage(listData.get(1).getImage());reInitialized();
     			
     		}
     	}
     	
     	else if(choiceButton.getName().equals("three"))
     	{
     		if(listData.get(2).hasKey())
     		{
     			Main.program.drawKey(listData.get(2).getKey());
     			reInitialized();
     		}
     		else
     		{
     			Main.program.drawImage(listData.get(2).getImage());
     			reInitialized();
     		}
     	}
     	
     	else if(buttonClicked.equals("Add Image"))
     	{
     		Data data = new Data(matcher.data.getPoints());
     		
   			String path = System.getProperty("user.dir");
   			JFileChooser chooser = new JFileChooser(path);
   			
   			int status = chooser.showOpenDialog(null);
   			if (status == JFileChooser.APPROVE_OPTION)
   	    {
   	        File file = chooser.getSelectedFile();
   	        path = file.getPath();
   	        data.setImage(path, Main.program.getXGap(), Main.program.getYGap());
   	        Main.database.add(data);
   	        JOptionPane.showMessageDialog(null, "Image was stored");
   	        Main.writeDatabase();
   	        reInitialized();
   	    }
   			else
   			{
   				JOptionPane.showMessageDialog(null, "Image was not stored");
   			}
     		
     	}
     	
     	else if(buttonClicked.equals("Add Key"))
     	{
     		Data data = new Data(matcher.data.getPoints());
   			String key = "Key";
   			String message = "Type in Key to associate\nwith image";
   			
   			while(key.length() != 1)
   			{
   				key = JOptionPane.showInputDialog(null, message);
   				message = "Key must be one character.\n Try again:";
   			}
   			
   			data.setKey(key.charAt(0));
   			Main.database.add(data);
   			Main.writeDatabase();
   			reInitialized();
     	}
     	
     	else if(buttonClicked.equals("Retry"))
     	{
     		reInitialized();
     	}
    }
		
	}
	
	private void setButtonText()
	{	
		listData = matcher.findTop();
		int num = 3 >listData.size() ? listData.size() : 3;
		clearScreenButton.setVisible(false);
		correlate.setVisible(false);
		loadButton.setVisible(false);

 		for(int i = 0; i < num; i++)
 		{
 			//show key if data has a key
 			if(listData.get(i).hasKey())
 			{
 				button[i].setText(listData.get(i).getKey() + "");
 				button[i].setVisible(true);
 			}
 			//else show the image
 			else
 			{
 				button[i].setIcon(Data.resizeImageIcon(listData.get(i).getImage(), 35, 25));
 				button[i].setVisible(true);
 				button[i].setPreferredSize(new Dimension(35, 25));
 			}
 		}
 		selection4.setVisible(true);
	}
	
	private void noMatch()
	{
		button[0].setVisible(false);
		button[1].setVisible(false);
		button[2].setVisible(false);
		addImageButton.setVisible(true);
		addKeyButton.setVisible(true);
		retryButton.setVisible(true);
		selection4.setVisible(false);
	}
	
	private void reInitialized()
	{
		loadButton.setVisible(true);
		clearScreenButton.setVisible(true);
		correlate.setVisible(true);
		button[0].setVisible(false);
		button[1].setVisible(false);
		button[2].setVisible(false);
		selection4.setVisible(false);
		addImageButton.setVisible(false);
		addKeyButton.setVisible(false);
		retryButton.setVisible(false);
		
		Main.recognizer.clearPanel();
	}
	
}
