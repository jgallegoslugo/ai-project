import java.awt.Point;
import java.util.List;
import java.util.ArrayList;

public class Database {
	private List<Point> points;
	private List<Double> distances;
	final static int initSize = 50;
	
	public Database()
	{
		points = new ArrayList<Point>(initSize);
		distances = new ArrayList<Double>(initSize);
	}
	public Database(List<Point> points, List<Double> distances)
	{
		this.points = points;
		this.distances = distances;
	}
	
	public List<Point> getPoints()
	{
		return points;
	}
	
	public List<Double> getDistances()
	{
		return distances;
	}
	
	public void setPoints(List<Point> points)
	{
		this.points = points; 
	}
	
	public void setDistances(List<Double> distances)
	{
		this.distances = distances;
	}
	
}
