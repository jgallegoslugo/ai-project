import java.util.ArrayList;
import java.util.List;

public class Matcher {
	
	List<Data> database;
	public Data data;
	SignalProcessing sp;
	ArrayList<ProcessedData> matches = new ArrayList<ProcessedData>();
	ArrayList<Data> datas = new ArrayList<Data>();
	
	public Matcher(Data data)
	{
		sp = new SignalProcessing();
		this.data = data;
		database = Main.database;
		findMatches();
	}
	
	public void findMatches()
	{
		for(int i = 0; i < database.size(); i++)
 		{
			double corr = sp.corr(0, data.getDistances(), database.get(i).getDistances());
 			if(corr > 0) 
 			{
 				ProcessedData pd = new ProcessedData(database.get(i), corr);
 				if(matches.size() == 0)
 				{
 					matches.add(pd);
 				}
 				else
 				{
 					if(matches.get(matches.size()-1).getCorr() >= corr )
 					{
 						matches.add(pd);
 					}
 					else
 					{
 						for(int j = 0; j < matches.size(); j++)
 						{
 							if(corr>=matches.get(j).getCorr())
 							{
 								matches.add(j, pd);
 								break;
 							}
 						}
 					}
 				}
 			}
 		}
	}
	
	public boolean hasMatch()
	{
		return matches.size()!=0;
	}
	public boolean hasTop(int num)
	{
		return matches.size()>=num;
	}
	
	
	public Data findBest()
	{
		return matches.get(0).getData();
	}
	
	public ArrayList<Data> findTop(int num)
	{
		for(int i =0; i < num; i++)
		{
			datas.add(matches.get(i).getData());
		}
		return datas;
	}
	
	public ArrayList<Data> findTop()
	{
		for(int i =0; i < matches.size(); i++)
		{
			if(matches.get(i).getCorr()>= 0.7)
			datas.add(matches.get(i).getData());
		}
		return datas;
	}
	
}