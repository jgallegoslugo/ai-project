
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class DatabaseButtonPanel extends JPanel{

	private static final long serialVersionUID = 1L;
	protected Data data = new Data();
	SignalProcessing sp;
	
	public DatabaseButtonPanel()
	{
		sp = new SignalProcessing();
		
		JButton addCharacterButton = new JButton("Add Character");
		JButton addImageButton = new JButton("Add Image");
		JButton addToDatabaseButton = new JButton("Add to Database");
		JButton clearScreenButton = new JButton("Clear Screen");
		JButton clearDatabaseButton = new JButton("Clear Database");
		
		add(addCharacterButton);
		add(addImageButton);
		add(addToDatabaseButton);
		add(clearScreenButton);
		add(clearDatabaseButton);
		
		ActionListener  buttonListener = new ButtonListener();
		 
		addCharacterButton.addActionListener(buttonListener);
		addImageButton.addActionListener(buttonListener);
		addToDatabaseButton.addActionListener(buttonListener);
		clearScreenButton.addActionListener(buttonListener);
		clearDatabaseButton.addActionListener(buttonListener);
	}
 
	private class ButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event) {
     	String buttonClicked = event.getActionCommand();
     	
     	if(buttonClicked.equals("Add Character"))
     	{
     		if(Main.creator.points.isEmpty())
     		{
     			JOptionPane.showMessageDialog(new JFrame(), "Draw in image on the screen first.");
     		}
     		else
     		{
     			data = new Data(Main.creator.points);
     			String key = "Key";
     			String message = "Type in Key to associate\nwith image";
     			
     			while(key.length() != 1)
     			{
     				key = JOptionPane.showInputDialog(null, message);
     				message = "Key must be one character.\n Try again:";
     			}
     			
     			data.setKey(key.charAt(0));
     			Main.database.add(data);
     		}
     	}
     	else if(buttonClicked.equals("Add Image"))
     	{     					
     		if(Main.creator.points.isEmpty())
     		{
     			JOptionPane.showMessageDialog(new JFrame(), "Draw in image on the screen first.");
     		}
     		else
     		{
     		  data = new Data(Main.creator.points);
     			String path = System.getProperty("user.dir");
     			JFileChooser chooser = new JFileChooser(path);
     			
     			int status = chooser.showOpenDialog(null);
     			if (status == JFileChooser.APPROVE_OPTION)
     	    {
     	        File file = chooser.getSelectedFile();
     	        path = file.getPath();
     	        data.setImage(path, Main.program.getXGap(), Main.program.getYGap());
     	        Main.database.add(data);
     	        JOptionPane.showMessageDialog(null, "Image was stored");
     	    }
     			else
     			{
     				JOptionPane.showMessageDialog(null, "Image was not stored");
     			}
     		}
     	}
     	else if(buttonClicked.equals("Add to Database"))
     	{
     		if(data.hasImage() || data.hasKey())
     		{
     			Main.creator.clearPanel();
     			Main.writeDatabase();
     			JOptionPane.showMessageDialog(null, "Item was added to database");
     		}
     		else
     		{
     			JOptionPane.showMessageDialog(null, "Add points and key or image first");
     		}
     	}
     	else if(buttonClicked.equals("Clear Screen"))
     	{
     		/*Data d = new Data();
     		String path = System.getProperty("user.dir") + "/images/bgImage.png";
     		d.setImage(path);
     		
     		ImageIcon img = new ImageIcon(path);
     		System.out.println(path);
     		Main.program.drawImage(img);*/
     		Main.creator.clearPanel();
     	}
     	else if(buttonClicked.equals("Clear Database"))
     	{
     		String confirmMessage = "Are you sure you want to delete the entire database?";
     		
     		int confirm = JOptionPane.showConfirmDialog (null, confirmMessage);
     		
     		if(confirm == JOptionPane.YES_OPTION)
     		{
     			Main.database.clear();
     			Main.clearDatabase();
     			JOptionPane.showMessageDialog(null, "Database was deleted.");
     		}
     		else
     		{
     			JOptionPane.showMessageDialog(null, "Database was not deleted.");
     		}
     	}     	
     	else if(buttonClicked.equals("Correlate"))
     	{
     		
     		System.out.println(Main.database.size());
     		Main.program.drawImage(Main.database.get(0).getImage());
     	}
		}
	}
}
