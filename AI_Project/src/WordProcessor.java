import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class WordProcessor extends JFrame{
	
	HashMap<Integer, Integer> mapInt = new HashMap<Integer, Integer>();
	//just cause we extened JFrame
	private static final long serialVersionUID = 1L;
	
	//key that was pressed or typed
	char key;
	
	//stuff needed for drawing
	private Graphics2D g2;
	private BufferedImage image;
	
	//variables to keep track of the positions of the cursor
	int posX = 15, posY = 15;
	public int gapX = 8;
	public int gapY = 15;
	int column;
	int row;
	int width;
	int height;
	
	//points that will tell us what point to draw in
	private ArrayList<Point> points;
	
	//position of the next point that will be drawn on.
	//Ex points.get(cursor) returns point where the next
	//character will be drawn
	int cursor = 0;
	
	//allows to check if key that was pressed is a special
	//key such as return key.
	final int BACKSPACE = KeyEvent.VK_BACK_SPACE;
	final int RETURN = KeyEvent.VK_ENTER;
	
	//just some panel stuff
	private ProgramButtonPanel buttonPanel;
	private JPanel panel;
	private Font font;
	
	public WordProcessor()
	{
		this(400, 400);
	}
	
	//constructor for WordProcessor
	public WordProcessor(int width, int height){
		setTitle("Word Processor");
		this.width = width;
		this.height = height;
		column=(width-posX)/gapX;
		row=(height-posY)/gapY;
		
		initPointsArray();
		buttonPanel=new ProgramButtonPanel();
		buttonPanel.setFocusable(false);
		image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		panel = new JPanel();
		panel.setFocusable(true);
		panel.addKeyListener(new KeyboardListener());
	
		g2 = (Graphics2D)image.getGraphics();
		g2.setColor(Color.BLACK);
		
		font = new Font(Font.MONOSPACED, Font.PLAIN, 1);
		
		g2.setFont(font);
		this.setFont(font);

		add(panel);
		this.setPreferredSize(new Dimension(width, height));
		this.requestFocus();
	}
	
	//this method initializes an array of points.
	//To get an idea of what this is doing, hit enter
	//at the start of the program so that the points are 
	//drawn on the screen. Hit backspace to delete all the points.
	//These are the points in the array.
	//Change the gap variable and rerun the program to see 
	//what this does to the points
	private void initPointsArray()
	{
		points= new ArrayList<Point>();
		
		//for each row
		for(int i = 0; i < row; i++)
		{
			posX = gapX;
			//for each column
			for(int j = 0; j < column; j++)
			{
				points.add(new Point(posX,posY));
				posX += gapX;
			}
			posX = gapY;
			posY += gapY;
		}
	}
	
	public void display()
  {
  	pack();
  	setVisible(true);
    revalidate();
  }
	
	public void closeFrame()
  {
  	dispose();
  }
  
  //this is a private class that listens for keys that are pressed
  //on the keyboard at any time.
	private class KeyboardListener extends KeyAdapter
	{
		//Check if any key was typed
		public void keyTyped(KeyEvent evt)
		{
			//get the key that was typed
			key = evt.getKeyChar();
			panel.requestFocus();
			
			//give us the screen to draw on
			Container c=(Container)evt.getSource();
	    g2 = (Graphics2D) c.getGraphics();

	    //if key is something that we can draw in the screen,
	    //then draw it
	    if(key != BACKSPACE && key != RETURN && !evt.isControlDown())
	    {
	    	g2.drawString(key + " ", points.get(cursor).x, points.get(cursor).y);
	    	cursor++;
	    }
	    
	    //if backspace was pressed and we have something to delete,
	    //delete it
	    if(cursor > 0 && key == BACKSPACE)
			{ 
				//update cursor position to the previous spot
	    		if(mapInt.containsKey(cursor))
	    		{
	    			int tempCursor = cursor;
	    			cursor = mapInt.get(cursor);
	    			mapInt.remove(tempCursor);
	    		}
	    		else
	    		{
	    			cursor--;
				
	    			//draw a rectangle with the background color over whatever 
	    			//is there. This is the easiest way to delete.
	    			g2.clearRect(points.get(cursor).x, points.get(cursor).y - gapY + 3, gapX + 2, gapY + 3);
	    		}
				
			}
	    
	    //if return key was pressed, send cursor to the new line
	    if(key == RETURN)
			{
	    		int tempCursor = cursor;
	    		cursor += (column - (cursor%column));
	    		mapInt.put(cursor, tempCursor);
			}
		}
	    
	    public void keyPressed(KeyEvent evt) 
	    {
	    	if (evt.isControlDown() && evt.getKeyCode() == KeyEvent.VK_C) 
	    	{   	
					Main.recognizer = new DrawingFrame(400, 400,new ProgramButtonPanel(), "Drawing Frame");
					Main.recognizer.setLocation(400,0);
					
					Main.recognizer.display(); 
				}

		}
	    
	}
	
	public void drawImage(ImageIcon img)
	{
		g2 = (Graphics2D)panel.getGraphics();
		
		if(img != null)
		{
			
			img.paintIcon(panel, g2, points.get(cursor).x, points.get(cursor).y - gapY + 2);
			cursor++;
		}
		else
		{
			String message = "Error: No image was associated with this data element";
			JOptionPane.showMessageDialog(null, message);
		}
	}
	public void drawKey(char key)
	{
		g2 = (Graphics2D)panel.getGraphics();
		
		g2.drawString(key + " ", points.get(cursor).x, points.get(cursor).y);
    cursor++;
	}
	public void drawKey(Data data)
	{
		if(data.hasKey())
		{
			g2.drawString(key + " ", points.get(cursor).x, points.get(cursor).y);
    	cursor++;
		}
		else
		{
			String message = "Error: No key was associated with this data element";
			JOptionPane.showMessageDialog(null, message);
		}
	}
	
	public int getXGap()
	{
		return gapX;
	}
	
	public int getYGap()
	{
		return gapY;
	}
	
	public Point getCursorPosition()
	{
		return new Point(points.get(cursor));
	}
}