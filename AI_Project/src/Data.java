
import java.awt.Image;
import java.awt.Point;
import java.util.List;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import java.io.Serializable;

public class Data implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Point> points;
	private List<Double> distances;
	private ImageIcon image;
	private int imageWidth;
	private int imageHeight;
	private char key;
	private final static int initSize = 500;
	private SignalProcessing sp = new SignalProcessing();
	
	public Data()
	{
		points = new ArrayList<Point>(initSize);
		distances = new ArrayList<Double>(initSize);
		image = null;
		key = 0; //key = null
	}
	public Data(List<Point> points)
	{
		this.points = points;
		this.distances = sp.distanceArray(points, sp.midPoint(points));
		image = null;
		key = 0; //key = null;
	}
	public Data(List<Point> points, List<Double> distances)
	{
		this.points = points;
		this.distances = distances;
		image = null;
		key = 0; //key = null
	}
	
	public List<Point> getPoints()
	{
		return points;
	}
	
	public List<Double> getDistances()
	{
		return distances;
	}
	
	public ImageIcon getImage()
	{
		return image;
	}
	
	public char getKey()
	{
		return key;
	}
	
	public void setPoints(List<Point> points)
	{
		this.points = points; 
	}
	
	public void setDistances(List<Double> distances)
	{
		this.distances = distances;
	}
	
	public void setImage(String path)
	{
 		setImage(path, imageWidth, imageHeight);
	}
	
	public void setImage(String path, int width, int height)
	{
		//set key to null first because you can only have 
		//a key or an image but not both
		this.key = 0;
 		
 		ImageIcon icon = new ImageIcon(path);
 		
 		//resize image if image size is not what we specified
 		if(icon.getIconWidth() != width && icon.getIconHeight() != height)
 			image = resizeImageIcon(icon, width, height);
 		else
 			image = icon;
	}
	
	public void drawImage(Point p)
	{
		drawImage(p.x, p.y);
	}
	
	public void drawImage(int width, int height)
	{
		if(image != null)
		{}	//WordProcessor.drawImage(image, width, height);
		else
		{
			String message = "Error: No image was associated with this data element";
			JOptionPane.showMessageDialog(null, message);
		}
	}
	
	public void setKey(char key)
	{
		//you can only have a key or an image but not both
		this.image = null;
		this.key = key;
	}
	public static ImageIcon resizeImageIcon(ImageIcon img, int newWidth, int newHeight)
	{
		return new ImageIcon(img.getImage().getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH));
	}
	
	public boolean hasKey()
	{
		return key != 0;
	}
	
	public boolean hasImage()
	{
		return image != null;
	}
	
	public boolean isEmpty()
	{	
		return points.isEmpty() && key == 0 && image == null;
	}
}
