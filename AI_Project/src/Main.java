import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;


public class Main {

	public static List<Data> database = new ArrayList<Data>();
	public static boolean saved = true;
	public static String databaseFile = System.getProperty("user.dir") + "database.aidb";
	public static ProgramButtonPanel programButtonPanel = new ProgramButtonPanel();
	public static DatabaseButtonPanel dbButtonPanel = new DatabaseButtonPanel();
	public static WordProcessor program = new WordProcessor(500, 500);
	public static DrawingFrame creator = new DrawingFrame(500, 500, dbButtonPanel, "Database Item Creator");
	public static DrawingFrame recognizer = new DrawingFrame(600, 600, programButtonPanel, "Drawing Pad");
	private static StartFrame start;
	
	public static void main(String[] args) 
	{
		start = new StartFrame();
		start.setVisible(true);
	}
	
	public static void readDatabase()
  {
      if(saved)
      {
          chooseFile(1);
          try
          {
              FileInputStream fis = new FileInputStream(databaseFile);
              ObjectInputStream in = new ObjectInputStream(fis);
              @SuppressWarnings("unchecked")
							List<Data> temp = (ArrayList<Data>)in.readObject();
              in.close();
              database=temp;
          }
          catch(ClassNotFoundException e)
          { 
              JOptionPane.showMessageDialog(null,e);
          }
          catch (IOException e) 
          { 
              JOptionPane.showMessageDialog(null,e);
          }
      }
      else
      {
         int confirm;
         confirm = JOptionPane.showConfirmDialog(null,"Do you want to save your work?");
         
         if(confirm == JOptionPane.YES_OPTION)
             writeDatabase();
         else if(confirm == JOptionPane.NO_OPTION)
         {
             saved = true;
             readDatabase();
         }
         else
             return;
      }
  }
  public static void writeDatabase() 
  {  
      chooseFile(2);
    	try
			{
				FileOutputStream fos = new FileOutputStream(databaseFile);
				ObjectOutputStream out = new ObjectOutputStream(fos);
        out.writeObject(database);
        out.close();
        saved = true;
			}	
    	catch(IOException e)	
      { 
           JOptionPane.showMessageDialog(null,e);
      }
  }
  
  public static void clearDatabase()
  {
  	try
  	{
	  	FileOutputStream fos = new FileOutputStream(databaseFile);
			ObjectOutputStream out = new ObjectOutputStream(fos);
	    out.writeObject(database);
	    out.close();
	    saved = true;
  	}
    catch(IOException e)
    {
    	
    }
  }
  public static void chooseFile(int ioOption)
  {
    int status, confirm;

    String  message = "Would you like to use the current default file: \n" + databaseFile;
    confirm = JOptionPane.showConfirmDialog (null, message);
    if (confirm == JOptionPane.YES_OPTION)
        return;
    
    JFileChooser chooser = new JFileChooser(System.getProperty("user.dir"));
    if (ioOption == 1)
        status = chooser.showOpenDialog (null);
    else
        status = chooser.showSaveDialog (null);
    if (status == JFileChooser.APPROVE_OPTION)
    {
        File file = chooser.getSelectedFile();
        databaseFile = file.getPath();
    }
  }
}
